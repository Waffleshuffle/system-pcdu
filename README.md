# Power Conditioning and Distribution Unit (PCDU)

This is a prototype of a power conditioning and distribution unit (PCDU) that
implements a full redundancy scheme and provides a CAN bus interface according
to the LibreCube board specification.

![](docs/pcdu.png)

## Getting Started

Find in the *source* folder the KiCAD source files of the board. In the *tests*
folder are the individual modules (Ex: battery charge module) for unit level testing.

Have a look at the [outstanding issues](https://gitlab.com/librecube/prototypes/system-pcdu/-/issues) to complete this prototype.
