EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 54 54
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 3500 2000 2    50   Input ~ 0
V_SA_M
Text HLabel 3500 2100 2    50   Input ~ 0
V_SRB_M
Text HLabel 3500 2200 2    50   Input ~ 0
V_BAT_M
Text HLabel 3500 2300 2    50   Input ~ 0
V_FRB_M
Text HLabel 3500 2400 2    50   Input ~ 0
I_SA_M
Text HLabel 3500 2500 2    50   Input ~ 0
I_SRB_M
Text HLabel 3500 2600 2    50   Input ~ 0
I_BCC_M
Text HLabel 3500 2700 2    50   Input ~ 0
I_BDC_M
Text HLabel 3500 2800 2    50   Input ~ 0
I_FRB_M
Text HLabel 3500 2900 2    50   Input ~ 0
I_FCL1_M
Text HLabel 3500 3000 2    50   Input ~ 0
I_FCL2_M
Text HLabel 3500 3100 2    50   Input ~ 0
I_LCL1_M
Text HLabel 3500 3200 2    50   Input ~ 0
I_LCL2_M
Text HLabel 3500 3300 2    50   Input ~ 0
I_LCL3_M
Text HLabel 3500 3400 2    50   Input ~ 0
I_LCL4_M
Text HLabel 3500 3500 2    50   Input ~ 0
I_LCL5_M
$Comp
L Connector_Generic:Conn_01x16 J3
U 1 1 5E7E4DF5
P 3100 2700
F 0 "J3" H 3018 3617 50  0000 C CNN
F 1 "Conn_01x16" H 3018 3526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 3100 2700 50  0001 C CNN
F 3 "~" H 3100 2700 50  0001 C CNN
	1    3100 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3500 2000 3300 2000
Wire Wire Line
	3500 2100 3300 2100
Wire Wire Line
	3500 2200 3300 2200
Wire Wire Line
	3500 2300 3300 2300
Wire Wire Line
	3500 2400 3300 2400
Wire Wire Line
	3500 2500 3300 2500
Wire Wire Line
	3500 2600 3300 2600
Wire Wire Line
	3500 2700 3300 2700
Wire Wire Line
	3500 2800 3300 2800
Wire Wire Line
	3500 2900 3300 2900
Wire Wire Line
	3500 3000 3300 3000
Wire Wire Line
	3500 3100 3300 3100
Wire Wire Line
	3300 3200 3500 3200
Wire Wire Line
	3300 3300 3500 3300
Wire Wire Line
	3500 3400 3300 3400
Wire Wire Line
	3300 3500 3500 3500
$EndSCHEMATC
