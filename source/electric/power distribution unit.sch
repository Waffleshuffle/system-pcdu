EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 3500 1500
Wire Wire Line
	3500 1100 3500 1500
Wire Wire Line
	2950 1500 3500 1500
$Comp
L power:+5V #PWR010
U 1 1 5E456139
P 3500 1100
F 0 "#PWR010" H 3500 950 50  0001 C CNN
F 1 "+5V" H 3500 1240 50  0000 C CNN
F 2 "" H 3500 1100 50  0000 C CNN
F 3 "" H 3500 1100 50  0000 C CNN
	1    3500 1100
	1    0    0    -1  
$EndComp
Text Label 3500 3250 1    60   ~ 0
Fully_regulated_bus
Text HLabel 2950 1500 0    50   Input ~ 0
5V
Wire Wire Line
	3500 2100 4900 2100
Wire Wire Line
	3500 1500 4900 1500
Wire Wire Line
	4400 3850 4900 3850
Wire Wire Line
	4400 3200 4900 3200
Text HLabel 4400 3200 0    50   Input ~ 0
LCL1_ON
Text HLabel 4400 3850 0    50   Input ~ 0
LCL2_ON
Connection ~ 3500 2100
Wire Wire Line
	3500 1500 3500 2100
Wire Wire Line
	3500 2100 3500 3000
Wire Wire Line
	4900 3650 3500 3650
Wire Wire Line
	4900 3000 3500 3000
Connection ~ 3500 3000
Wire Wire Line
	3500 3000 3500 3650
$Sheet
S 4900 2900 750  400 
U 5E45616A
F0 "latching current limiter 1" 60
F1 "latching current limiter.sch" 60
F2 "IN" I L 4900 3000 60 
F3 "OUT" I R 5650 3100 60 
F4 "ON" I L 4900 3200 60 
$EndSheet
$Sheet
S 4900 1400 600  300 
U 5E4560E3
F0 "foldback current limiter 1" 60
F1 "foldback current limiter.sch" 60
F2 "IN" I L 4900 1500 60 
F3 "OUT" O R 5500 1550 60 
$EndSheet
Text HLabel 7650 3850 2    50   Output ~ 0
LCL2_OUT
Text HLabel 7650 3650 2    50   Output ~ 0
I_LCL2_M
Text HLabel 7650 3200 2    50   Output ~ 0
LCL1_OUT
Text HLabel 7650 3000 2    50   Output ~ 0
I_LCL1_M
Text HLabel 7650 2250 2    50   Output ~ 0
FCL2_OUT
Text HLabel 7650 2050 2    50   Output ~ 0
I_FCL2_M
Text HLabel 7650 1650 2    50   Output ~ 0
FCL1_OUT
Text HLabel 7650 1450 2    50   Output ~ 0
I_FCL1_M
$Sheet
S 4900 2000 600  300 
U 5E45616E
F0 "foldback current limiter 2" 60
F1 "foldback current limiter.sch" 60
F2 "IN" I L 4900 2100 60 
F3 "OUT" O R 5500 2150 60 
$EndSheet
$Sheet
S 4900 3550 750  400 
U 5E456165
F0 "latching current limiter 2" 60
F1 "latching current limiter.sch" 60
F2 "IN" I L 4900 3650 60 
F3 "OUT" I R 5650 3750 60 
F4 "ON" I L 4900 3850 60 
$EndSheet
Wire Wire Line
	7200 3200 7650 3200
Wire Wire Line
	7200 3850 7650 3850
Wire Wire Line
	7200 1650 7650 1650
Wire Wire Line
	7200 2250 7650 2250
$Sheet
S 6700 2950 500  300 
U 5E456132
F0 "measure LCL1 current" 50
F1 "measure current.sch" 50
F2 "IN" I L 6700 3100 50 
F3 "MEAS" O R 7200 3000 50 
F4 "OUT" O R 7200 3200 50 
$EndSheet
$Sheet
S 6700 1400 500  300 
U 5E45612D
F0 "measure FCL1 current" 50
F1 "measure current.sch" 50
F2 "IN" I L 6700 1550 50 
F3 "MEAS" O R 7200 1450 50 
F4 "OUT" O R 7200 1650 50 
$EndSheet
$Sheet
S 6700 2000 500  300 
U 5E456128
F0 "measure FCL2 current" 50
F1 "measure current.sch" 50
F2 "IN" I L 6700 2150 50 
F3 "MEAS" O R 7200 2050 50 
F4 "OUT" O R 7200 2250 50 
$EndSheet
$Sheet
S 6700 3600 500  300 
U 5E456123
F0 "measure LCL2 current" 50
F1 "measure current.sch" 50
F2 "IN" I L 6700 3750 50 
F3 "MEAS" O R 7200 3650 50 
F4 "OUT" O R 7200 3850 50 
$EndSheet
Wire Wire Line
	7650 1450 7200 1450
Wire Wire Line
	7650 2050 7200 2050
Wire Wire Line
	7650 3000 7200 3000
Wire Wire Line
	7650 3650 7200 3650
Wire Wire Line
	5500 1550 6700 1550
Wire Wire Line
	5500 2150 6700 2150
Wire Wire Line
	5650 3100 6700 3100
Wire Wire Line
	5650 3750 6700 3750
$EndSCHEMATC
