EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 19 41
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3650 2700 950  350 
U 5E4DE9B4
F0 "solar array upconverter B" 60
F1 "boost converter.sch" 60
F2 "IN" I L 3650 2850 60 
F3 "OUT" O R 4600 2850 60 
$EndSheet
Text Label 6300 2200 0    60   ~ 0
Semi_Regulated_Bus
$Comp
L Device:D_Schottky D?
U 1 1 5E4DE9C1
P 6150 2450
AR Path="/5E4DE9C1" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9C1" Ref="D14"  Part="1" 
F 0 "D14" H 6150 2550 50  0000 C CNN
F 1 "D_Schottky" H 6150 2350 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 6150 2450 50  0001 C CNN
F 3 "" H 6150 2450 50  0000 C CNN
	1    6150 2450
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DE9CB
P 6850 2500
AR Path="/5E4DE9CB" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9CB" Ref="D15"  Part="1" 
F 0 "D15" H 6850 2600 50  0000 C CNN
F 1 "D_Schottky" H 6850 2400 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 6850 2500 50  0001 C CNN
F 3 "" H 6850 2500 50  0000 C CNN
	1    6850 2500
	0    -1   1    0   
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DE9D1
P 7100 2500
AR Path="/5E4DE9D1" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9D1" Ref="D16"  Part="1" 
F 0 "D16" H 7100 2600 50  0000 C CNN
F 1 "D_Schottky" H 7100 2400 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 7100 2500 50  0001 C CNN
F 3 "" H 7100 2500 50  0000 C CNN
	1    7100 2500
	0    -1   1    0   
$EndComp
Wire Wire Line
	7100 2350 6950 2350
Wire Wire Line
	6300 2450 6300 2200
$Comp
L Connector:Conn_01x02_Male P?
U 1 1 5E4DE9DE
P 7350 4950
AR Path="/5E4DE9DE" Ref="P?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9DE" Ref="P7"  Part="1" 
F 0 "P7" H 7350 5100 50  0000 C CNN
F 1 "CONN_01X02" V 7450 4950 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7350 4950 50  0001 C CNN
F 3 "" H 7350 4950 50  0000 C CNN
	1    7350 4950
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male P?
U 1 1 5E4DE9E4
P 7350 5450
AR Path="/5E4DE9E4" Ref="P?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9E4" Ref="P8"  Part="1" 
F 0 "P8" H 7350 5600 50  0000 C CNN
F 1 "CONN_01X02" V 7450 5450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7350 5450 50  0001 C CNN
F 3 "" H 7350 5450 50  0000 C CNN
	1    7350 5450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E4DE9EA
P 7150 5450
AR Path="/5E4DE9EA" Ref="#PWR?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9EA" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 7150 5200 50  0001 C CNN
F 1 "GND" H 7150 5300 50  0000 C CNN
F 2 "" H 7150 5450 50  0000 C CNN
F 3 "" H 7150 5450 50  0000 C CNN
	1    7150 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E4DE9F0
P 7150 4950
AR Path="/5E4DE9F0" Ref="#PWR?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9F0" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 7150 4700 50  0001 C CNN
F 1 "GND" H 7150 4800 50  0000 C CNN
F 2 "" H 7150 4950 50  0000 C CNN
F 3 "" H 7150 4950 50  0000 C CNN
	1    7150 4950
	1    0    0    -1  
$EndComp
$Sheet
S 8650 2150 700  300 
U 5E4DE9F4
F0 "SRB upconverter A" 60
F1 "boost converter.sch" 60
F2 "IN" I L 8650 2300 60 
F3 "OUT" O R 9350 2300 60 
$EndSheet
$Sheet
S 7700 2100 650  850 
U 5E4DE9FE
F0 "system power switch" 60
F1 "system power switch.sch" 60
F2 "IN" I L 7700 2200 60 
F3 "OUT" I R 8350 2300 60 
F4 "S0" I L 7700 2350 60 
F5 "S2" I L 7700 2550 60 
F6 "S4" I L 7700 2750 60 
F7 "S1" I L 7700 2450 60 
F8 "S3" I L 7700 2650 60 
F9 "S5" I L 7700 2850 60 
$EndSheet
$Sheet
S 8650 2700 700  300 
U 5E4DEA02
F0 "SRB upconverter B" 60
F1 "boost converter.sch" 60
F2 "IN" I L 8650 2850 60 
F3 "OUT" O R 9350 2850 60 
$EndSheet
Wire Wire Line
	6850 5350 7000 5350
$Comp
L power:+BATT #PWR?
U 1 1 5E4DEA0D
P 7150 4850
AR Path="/5E4DEA0D" Ref="#PWR?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA0D" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 7150 4700 50  0001 C CNN
F 1 "+BATT" H 7150 4990 50  0000 C CNN
F 2 "" H 7150 4850 50  0000 C CNN
F 3 "" H 7150 4850 50  0000 C CNN
	1    7150 4850
	1    0    0    -1  
$EndComp
NoConn ~ 7700 2350
NoConn ~ 7700 2450
Wire Wire Line
	7500 2550 7700 2550
Wire Wire Line
	7500 2650 7700 2650
Wire Wire Line
	7500 2750 7700 2750
Wire Wire Line
	7500 2850 7700 2850
Wire Wire Line
	6850 4850 7150 4850
Wire Wire Line
	2000 4750 2000 5250
Wire Wire Line
	1800 2550 1800 2750
Wire Wire Line
	2000 2550 2000 3100
Wire Wire Line
	1800 3100 1800 3300
Wire Wire Line
	2000 3100 2000 3650
Wire Wire Line
	1800 3650 1800 3850
Wire Wire Line
	2000 3650 2000 4200
Wire Wire Line
	1800 4200 1800 4400
Wire Wire Line
	2000 4200 2000 4750
Wire Wire Line
	1800 2000 1800 2200
Wire Wire Line
	1800 4750 1800 4950
Connection ~ 2000 4750
Connection ~ 1800 2550
Connection ~ 2000 2550
Wire Wire Line
	1800 2550 2000 2550
Connection ~ 1800 3100
Connection ~ 2000 3100
Wire Wire Line
	1800 3100 2000 3100
Connection ~ 1800 3650
Connection ~ 2000 3650
Wire Wire Line
	1800 3650 2000 3650
Connection ~ 1800 4200
Connection ~ 2000 4200
Wire Wire Line
	1800 4200 2000 4200
Connection ~ 1800 2000
Connection ~ 1800 4750
Wire Wire Line
	2000 4750 1800 4750
Wire Wire Line
	1800 2000 2000 2000
Wire Wire Line
	1800 4650 1800 4750
Wire Wire Line
	1800 4100 1800 4200
Wire Wire Line
	1800 3550 1800 3650
Wire Wire Line
	1800 3000 1800 3100
Wire Wire Line
	1800 2450 1800 2550
Wire Wire Line
	1800 1900 1800 2000
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA48
P 1650 4950
AR Path="/5E4DEA48" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA48" Ref="D12"  Part="1" 
F 0 "D12" H 1650 5050 50  0000 C CNN
F 1 "D_Schottky" H 1650 4850 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 4950 50  0001 C CNN
F 3 "" H 1650 4950 50  0000 C CNN
	1    1650 4950
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA4E
P 1650 4650
AR Path="/5E4DEA4E" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA4E" Ref="D11"  Part="1" 
F 0 "D11" H 1650 4750 50  0000 C CNN
F 1 "D_Schottky" H 1650 4550 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 4650 50  0001 C CNN
F 3 "" H 1650 4650 50  0000 C CNN
	1    1650 4650
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA54
P 1650 4400
AR Path="/5E4DEA54" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA54" Ref="D10"  Part="1" 
F 0 "D10" H 1650 4500 50  0000 C CNN
F 1 "D_Schottky" H 1650 4300 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 4400 50  0001 C CNN
F 3 "" H 1650 4400 50  0000 C CNN
	1    1650 4400
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA5A
P 1650 4100
AR Path="/5E4DEA5A" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA5A" Ref="D9"  Part="1" 
F 0 "D9" H 1650 4200 50  0000 C CNN
F 1 "D_Schottky" H 1650 4000 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 4100 50  0001 C CNN
F 3 "" H 1650 4100 50  0000 C CNN
	1    1650 4100
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA60
P 1650 3850
AR Path="/5E4DEA60" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA60" Ref="D8"  Part="1" 
F 0 "D8" H 1650 3950 50  0000 C CNN
F 1 "D_Schottky" H 1650 3750 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 3850 50  0001 C CNN
F 3 "" H 1650 3850 50  0000 C CNN
	1    1650 3850
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA66
P 1650 3550
AR Path="/5E4DEA66" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA66" Ref="D7"  Part="1" 
F 0 "D7" H 1650 3650 50  0000 C CNN
F 1 "D_Schottky" H 1650 3450 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 3550 50  0001 C CNN
F 3 "" H 1650 3550 50  0000 C CNN
	1    1650 3550
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA6C
P 1650 3300
AR Path="/5E4DEA6C" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA6C" Ref="D6"  Part="1" 
F 0 "D6" H 1650 3400 50  0000 C CNN
F 1 "D_Schottky" H 1650 3200 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 3300 50  0001 C CNN
F 3 "" H 1650 3300 50  0000 C CNN
	1    1650 3300
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA72
P 1650 3000
AR Path="/5E4DEA72" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA72" Ref="D5"  Part="1" 
F 0 "D5" H 1650 3100 50  0000 C CNN
F 1 "D_Schottky" H 1650 2900 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 3000 50  0001 C CNN
F 3 "" H 1650 3000 50  0000 C CNN
	1    1650 3000
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA78
P 1650 2750
AR Path="/5E4DEA78" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA78" Ref="D4"  Part="1" 
F 0 "D4" H 1650 2850 50  0000 C CNN
F 1 "D_Schottky" H 1650 2650 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 2750 50  0001 C CNN
F 3 "" H 1650 2750 50  0000 C CNN
	1    1650 2750
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA7E
P 1650 2450
AR Path="/5E4DEA7E" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA7E" Ref="D3"  Part="1" 
F 0 "D3" H 1650 2550 50  0000 C CNN
F 1 "D_Schottky" H 1650 2350 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 2450 50  0001 C CNN
F 3 "" H 1650 2450 50  0000 C CNN
	1    1650 2450
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA84
P 1650 2200
AR Path="/5E4DEA84" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA84" Ref="D2"  Part="1" 
F 0 "D2" H 1650 2300 50  0000 C CNN
F 1 "D_Schottky" H 1650 2100 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 2200 50  0001 C CNN
F 3 "" H 1650 2200 50  0000 C CNN
	1    1650 2200
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5E4DEA8A
P 1650 1900
AR Path="/5E4DEA8A" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DEA8A" Ref="D1"  Part="1" 
F 0 "D1" H 1650 2000 50  0000 C CNN
F 1 "D_Schottky" H 1650 1800 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 1650 1900 50  0001 C CNN
F 3 "" H 1650 1900 50  0000 C CNN
	1    1650 1900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1500 1900 1500 2000
Wire Wire Line
	1500 2450 1500 2550
Wire Wire Line
	1500 3000 1500 3100
Wire Wire Line
	1500 3550 1500 3650
Wire Wire Line
	1500 4650 1500 4750
Connection ~ 1500 2000
Wire Wire Line
	1500 2000 1500 2200
Connection ~ 1500 2550
Wire Wire Line
	1500 2550 1500 2750
Connection ~ 1500 3100
Wire Wire Line
	1500 3100 1500 3300
Connection ~ 1500 3650
Wire Wire Line
	1500 3650 1500 3850
Connection ~ 1500 4750
Wire Wire Line
	1500 4750 1500 4950
Wire Wire Line
	1500 4200 1500 4400
Wire Wire Line
	1500 4100 1500 4200
Connection ~ 1500 4200
$Sheet
S 4900 1950 500  300 
U 5E4DEAA1
F0 "measure SRB current" 50
F1 "measure current.sch" 50
F2 "IN" I L 4900 2100 50 
F3 "MEAS" O R 5400 2000 50 
F4 "OUT" O R 5400 2200 50 
$EndSheet
Wire Wire Line
	6000 2450 6000 2200
$Sheet
S 5700 4700 500  300 
U 5E4DEAC9
F0 "measure battery charge current" 50
F1 "measure current.sch" 50
F2 "IN" I L 5700 4850 50 
F3 "MEAS" O R 6200 4750 50 
F4 "OUT" O R 6200 4950 50 
$EndSheet
$Sheet
S 6700 3850 300  500 
U 5E4DEACE
F0 "measure battery discharge current" 50
F1 "measure current.sch" 50
F2 "IN" I B 6850 4350 50 
F3 "MEAS" O T 6750 3850 50 
F4 "OUT" O T 6950 3850 50 
$EndSheet
Wire Wire Line
	5700 4850 5650 4850
Wire Wire Line
	5650 4850 5650 4500
Wire Wire Line
	6200 4950 6850 4950
Wire Wire Line
	6850 4950 6850 5350
Wire Wire Line
	6850 4350 6850 4850
Connection ~ 6850 4950
Connection ~ 6850 4850
Wire Wire Line
	6850 4850 6850 4950
Wire Wire Line
	6950 2350 6950 2200
Connection ~ 6950 2350
Wire Wire Line
	6950 2350 6850 2350
Wire Wire Line
	1200 2000 1500 2000
Wire Wire Line
	1200 3100 1500 3100
Wire Wire Line
	1200 3650 1500 3650
Wire Wire Line
	1200 4200 1500 4200
Wire Wire Line
	1200 4750 1500 4750
Wire Wire Line
	1200 2550 1500 2550
$Sheet
S 6950 5800 500  300 
U 5E4DEAF7
F0 "measure battery voltage" 50
F1 "measure bat voltage.sch" 50
F2 "MEAS" O R 7450 5850 50 
F3 "IN" I L 6950 5950 50 
$EndSheet
$Sheet
S 9650 2700 500  300 
U 5E4DEAFB
F0 "measure FRB voltage" 50
F1 "measure voltage.sch" 50
F2 "IN" I L 9650 2850 50 
F3 "MEAS" O R 10150 2750 50 
$EndSheet
$Sheet
S 4900 1500 500  300 
U 5E4DEAFF
F0 "measure SRB voltage" 50
F1 "measure voltage.sch" 50
F2 "IN" I L 4900 1650 50 
F3 "MEAS" O R 5400 1550 50 
$EndSheet
Wire Wire Line
	6950 5950 6850 5950
Wire Wire Line
	6850 5950 6850 5350
Connection ~ 6850 5350
Wire Wire Line
	5700 2000 5400 2000
Wire Wire Line
	5700 1550 5400 1550
Wire Wire Line
	6400 4750 6200 4750
Wire Wire Line
	6750 3650 6750 3850
$Sheet
S 9650 2150 500  300 
U 5E4DEB20
F0 "measure FRB current" 50
F1 "measure current.sch" 50
F2 "IN" I L 9650 2300 50 
F3 "MEAS" O R 10150 2200 50 
F4 "OUT" O R 10150 2400 50 
$EndSheet
Text HLabel 1200 2000 0    50   Input ~ 0
SAXP
Text HLabel 1200 2550 0    50   Input ~ 0
SAXM
Text HLabel 1200 3100 0    50   Input ~ 0
SAYP
Text HLabel 1200 3650 0    50   Input ~ 0
SAYM
Text HLabel 1200 4200 0    50   Input ~ 0
SAZP
Text HLabel 1200 4750 0    50   Input ~ 0
SAZM
Wire Wire Line
	1200 5250 2000 5250
Text HLabel 1200 5250 0    50   Input ~ 0
CHARGE
Wire Wire Line
	2000 2000 2000 2550
Wire Wire Line
	3000 1900 2700 1900
Text HLabel 3000 1900 2    50   Output ~ 0
I_SA_M
Wire Wire Line
	2200 2000 2000 2000
Connection ~ 2000 2000
Wire Wire Line
	2200 2550 2000 2550
Wire Wire Line
	2700 2450 3000 2450
Text HLabel 3000 2450 2    50   Output ~ 0
V_SA_M
Wire Wire Line
	6850 2650 6950 2650
Wire Wire Line
	3500 2850 3500 2100
Wire Wire Line
	3500 2100 2700 2100
Wire Wire Line
	3650 2850 3500 2850
Wire Wire Line
	3650 2100 3500 2100
Connection ~ 3500 2100
Wire Wire Line
	4600 2100 4750 2100
Wire Wire Line
	4750 2100 4750 2850
Wire Wire Line
	4750 2850 4600 2850
Wire Wire Line
	4900 2100 4750 2100
Connection ~ 4750 2100
Wire Wire Line
	4900 1650 4750 1650
Wire Wire Line
	4750 1650 4750 2100
Text HLabel 5700 1550 2    50   Output ~ 0
V_SRB_M
Text HLabel 5700 2000 2    50   Output ~ 0
I_SRB_M
Text HLabel 6400 4750 2    50   Output ~ 0
I_BCC_M
Text HLabel 6750 3650 1    50   Output ~ 0
I_BDC_M
Text HLabel 7650 5850 2    50   Output ~ 0
V_BAT_M
Wire Wire Line
	7450 5850 7650 5850
Connection ~ 6950 2200
Wire Wire Line
	6300 2200 6950 2200
Wire Wire Line
	6950 2650 6950 3850
Connection ~ 6950 2650
Wire Wire Line
	6950 2650 7100 2650
Text HLabel 7500 2550 0    50   Input ~ 0
S2
Text HLabel 7500 2650 0    50   Input ~ 0
S3
Text HLabel 7500 2750 0    50   Input ~ 0
S4
Text HLabel 7500 2850 0    50   Input ~ 0
S5
Wire Wire Line
	6950 2200 7700 2200
Wire Wire Line
	9350 2300 9500 2300
Wire Wire Line
	9500 2300 9500 2850
Wire Wire Line
	9500 2850 9350 2850
Wire Wire Line
	8500 2300 8500 2850
Wire Wire Line
	8500 2850 8650 2850
Wire Wire Line
	8500 2300 8650 2300
Connection ~ 8500 2300
Wire Wire Line
	10400 2400 10150 2400
Wire Wire Line
	10400 2200 10150 2200
Wire Wire Line
	10400 2750 10150 2750
Wire Wire Line
	9650 2850 9500 2850
Connection ~ 9500 2850
Wire Wire Line
	9500 2300 9650 2300
Connection ~ 9500 2300
Text HLabel 10400 2200 2    50   Output ~ 0
I_FRB_M
Text HLabel 10400 2400 2    50   Output ~ 0
5V
Text HLabel 10400 2750 2    50   Output ~ 0
V_FRB_M
Wire Wire Line
	8350 2300 8500 2300
Wire Wire Line
	5400 2200 5650 2200
$Sheet
S 5750 3400 350  950 
U 5E4DE9C5
F0 "battery charge regulator B" 60
F1 "battery charge regulator.sch" 60
F2 "IN" I T 5950 3400 60 
F3 "OUT" O B 5900 4350 60 
$EndSheet
Wire Wire Line
	5350 4350 5350 4500
Wire Wire Line
	5350 4500 5650 4500
Wire Wire Line
	5650 4500 5900 4500
Wire Wire Line
	5900 4350 5900 4500
Connection ~ 5650 4500
Wire Wire Line
	5400 3400 5400 3250
Wire Wire Line
	5400 3250 5650 3250
Wire Wire Line
	5950 3250 5950 3400
Connection ~ 5650 2200
Wire Wire Line
	5650 2200 6000 2200
Wire Wire Line
	5650 2200 5650 3250
Connection ~ 5650 3250
Wire Wire Line
	5650 3250 5950 3250
Connection ~ 7150 4850
Connection ~ 6000 2200
Connection ~ 6300 2200
$Comp
L Device:D_Schottky D?
U 1 1 5E4DE9BB
P 6150 2200
AR Path="/5E4DE9BB" Ref="D?"  Part="1" 
AR Path="/5E4D81A8/5E4DE9BB" Ref="D13"  Part="1" 
F 0 "D13" H 6150 2300 50  0000 C CNN
F 1 "D_Schottky" H 6150 2100 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 6150 2200 50  0001 C CNN
F 3 "" H 6150 2200 50  0000 C CNN
	1    6150 2200
	-1   0    0    -1  
$EndComp
$Sheet
S 2200 2400 500  300 
U 5E4DEAF3
F0 "measure solar array voltage" 50
F1 "measure voltage.sch" 50
F2 "IN" I L 2200 2550 50 
F3 "MEAS" O R 2700 2450 50 
$EndSheet
$Sheet
S 2200 1850 500  300 
U 5E4DEAC4
F0 "measure solar array current" 50
F1 "measure current.sch" 50
F2 "IN" I L 2200 2000 50 
F3 "MEAS" O R 2700 1900 50 
F4 "OUT" O R 2700 2100 50 
$EndSheet
$Sheet
S 3650 1950 950  350 
U 5E4DEA07
F0 "solar array upconverter A" 60
F1 "boost converter.sch" 60
F2 "IN" I L 3650 2100 60 
F3 "OUT" O R 4600 2100 60 
$EndSheet
$Sheet
S 5200 3400 350  950 
U 5E4DE9D8
F0 "battery charge regulator A" 60
F1 "battery charge regulator.sch" 60
F2 "IN" I T 5400 3400 60 
F3 "OUT" O B 5350 4350 60 
$EndSheet
$Comp
L power:PWR_FLAG #FLG0129
U 1 1 5EA2A62E
P 7000 5350
F 0 "#FLG0129" H 7000 5425 50  0001 C CNN
F 1 "PWR_FLAG" H 7000 5523 50  0000 C CNN
F 2 "" H 7000 5350 50  0001 C CNN
F 3 "~" H 7000 5350 50  0001 C CNN
	1    7000 5350
	1    0    0    -1  
$EndComp
Connection ~ 7000 5350
Wire Wire Line
	7000 5350 7150 5350
$EndSCHEMATC
